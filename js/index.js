$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval:3000
			});
			$('#informacion').on('shown.bs.modal', function (e){
				console.log('el Modal esta Activo');
				$('#infoBtn').removeClass('btn-outline-success');
				$('#infoBtn').addClass('btn-primary');
				$('#infoBtn').prop('disabled', true);
			});
			$('#informacion').on('hidden.bs.modal', function (e){
				console.log('el Modal se ocultó');
				$('#infoBtn').removeClass('btn-primary');
				$('#infoBtn').addClass('btn-outline-success');
				$('#infoBtn').prop('disabled', false);
			});
		});